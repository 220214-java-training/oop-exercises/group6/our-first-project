package dev.scott.fantasyAnimals;

public class Driver {
    int flightSpeed;
    int magicLevel;
    int intelligence;

    public static void main(String[] args){
        Dragon saphira = new Dragon();
        // Added values to properties - Jazib
        saphira.flightSpeed = 1000;
        saphira.magicLevel = 1000;
        saphira.intelligence = 1000;
            System.out.println("Flight Speed: " + saphira.flightSpeed);
            System.out.println("Magic Level: " + saphira.magicLevel);
            System.out.println("Intelligence: " + saphira.intelligence);

        Unicorn sparkle = new Unicorn();
        sparkle.flightSpeed = 0;
        sparkle.magicLevel = 1000;
        sparkle.intelligence = 600;
            System.out.println("Flight Speed: " + sparkle.flightSpeed);
            System.out.println("Magic Level: " + sparkle.magicLevel);
            System.out.println("Intelligence: " + sparkle.intelligence);

        Pegasus ignatius = new Pegasus();
        ignatius.flightSpeed = 0;
        ignatius.magicLevel = 1000;
        ignatius.intelligence = 600;
        System.out.println("Flight Speed: " + ignatius.flightSpeed);
        System.out.println("Magic Level: " + ignatius.magicLevel);
        System.out.println("Intelligence: " + ignatius.intelligence);
}
}
