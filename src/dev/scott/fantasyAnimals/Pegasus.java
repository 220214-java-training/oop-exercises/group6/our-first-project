package dev.scott.fantasyAnimals;

public class Pegasus extends fantasyAnimals{

    Pegasus(){
        super();
    }
    public int flightSpeed = 50;
    public int magicLevel = 350;
    public int intelligence = 9000;
    public boolean canTeleport = true;
}

// used super() in this class because it broke wherever else I attempted to use it.